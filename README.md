WHAT
====

Generate a Debian-style changelog from git, where tags are expected to
represent versions, with a bullet point for each commit subject since
the previous version.

There's a decent chance I've messed something up here.  I might fix it if I
wind up actually using this, otherwise I'd advise checking my work before
using this script for anything too important.

See the POD in [git-changelog](git-changelog) for more info.

CONTEXT / BACKGROUND
====================

Context: https://nymwars.org/@sungo/105794545540526553

There's also a Python tool called [`git-changelog`][python-gcl], although
I don't think it does Debian-style changelogs out of the box.

You may also be looking for `git-dch` from `git-buildpackage`:

  - https://honk.sigxcpu.org/piki/projects/git-buildpackage/
  - https://wikitech.wikimedia.org/wiki/Git-buildpackage

At the moment, I don't know how to use either of those things, so good luck.

[python-gcl]: https://pypi.org/project/git-changelog/

INSTALLING
==========

This shells out to `git`, so you'll need a working Git installation.

You'll need a working, relatively recent Perl.  I don't think it has any Perl
dependencies outside of the standard library.

Otherwise, copy `git-changelog` to somewhere in your path.

I'll package this up for CPAN at some point.  Probably.

USAGE
=====

For a checkout of https://code.p1k3.com/gitea/brennen/wrt this produces output
like:

```
$ git-changelog --project wrt --distribution somedistro | head -30
wrt (v7.1.0) somedistro; urgency=high

   * handle sites with an index file in the root

 -- Brennen Bearnes <code@p1k3.com>  Sat, 18 Apr 2020 23:17:44 -0600

wrt (v7.0.0) somedistro; urgency=high

   * cache rendered html; extract titles; all as utf-8
   * move elapsed rendering time into bin/wrt-render-all
   * Util: croak on utf-8 warnings
   * add a full tagging system to wrt
   * pull entry sorting into App::WRT::Sort & generalize
   * sort tagged entries with alpha first then reverse chronological
   * stub out a wrt-repl for debugging
   * $entrypath_expr to $EntryStore::RENDERABLE_EXPR & add methods
   * App::WRT: minor code formatting tweaks for line length, etc.
   * handle(): use POSIX character classes instead of a-z
   * wrt-repl: more useful startup, add Data::Dumper
   * wrt-repl: slightly better comments / POD
   * wrt-feed: disentangles feed printing from WRT::display()
   * stub a test for bin/wrt-feed
   * add JSON Feed support; make Atom feed use caches; decode utf-8 output
   * json feeds: add home_page_url and (optionally) favicon
   * add rfc_3339_date() and use it for json feed output
   * add feed links to wrt-init template and examples
   * bin/wrt-feed: add --json option; handle @local_argv correctly
   * update changelog & do some pre-release tidying

 -- Brennen Bearnes <code@p1k3.com>  Sat, 4 Apr 2020 23:44:04 -0600
```

...which looks mostly right, but I might be getting a little punchy.

COPYING
=======

Knock yourself out.

To the extent possible under law, Brennen Bearnes has waived all copyright and
related or neighboring rights to git-changelog. 
